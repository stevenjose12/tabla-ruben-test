<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function get(Request $request){
        $users = User::whereNull('deleted_at');
        if(isset($request->name)){
            $users = $users->where('name', 'like', '%' . $request->name . '%')
                ->orWhere('email', 'like', '%' . $request->name . '%');
        }

        if(isset($request->level)){
            $users = $users->where('level', $request->level);
        }

        if(isset($request->status)){
            $users = $users->where('status', $request->status);
        }

        $users = $users->paginate(10);
        return response()->json([
            'users' => $users
        ]);
    }

    public function update(Request $request){
        $user = User::find($request->id);
        if($user){
            $user->status = $request->status;
            $user->save();
            return response()->json([
                'result' => true
            ]);
        }else{
            return response()->json([
                'error' => 'El usuario no ha sido encontrado'
            ], 500);
        }
    }

    public function delete(Request $request){
        $user = User::find($request->id);
        if($user){
            $user->delete();
            return response()->json([
                'result' => true
            ]);
        }else{
            return response()->json([
                'error' => 'El usuario no ha sido encontrado'
            ], 500);
        }
    }
}
