<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use \Illuminate\Database\Eloquent\SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $appends = [
        'status_text',
        'level_text',
    ];

    public function getStatusTextAttribute(){
        switch ($this->status) {
            case 1:
                return 'Activo';
            case 0:
                return 'Inactivo';
        }
    }
    public function getLevelTextAttribute(){
        switch ($this->level) {
            case 1:
                return 'Admin';
            case 0:
                return 'Usuario';
        }
    }
}
