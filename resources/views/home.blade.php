@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <users-table user="{{json_encode(Auth::user())}}" url="{{url('/')}}"></users-table>
        </div>
    </div>
</div>
@endsection
