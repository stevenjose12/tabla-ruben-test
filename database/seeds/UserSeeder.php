<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
            ->insert([
                'name' => 'Admin',
                'email' =>'admin@mail.com',
                'password' => Hash::make('123456'),
                'level' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:m:s')
            ]);
        $limit = 20000;
        for ($i=0; $i < $limit; $i++) {
            DB::table('users')
                ->insert([
                    'name' => 'Usuario - '.$i,
                    'email' =>'usuario'.$i.'@mail.com',
                    'password' => Hash::make('usuario'.$i),
                    'status' => $i < ($limit / 2) ? 1 : 0,
                    'created_at' => Carbon::now()->format('Y-m-d H:m:s')
                ]);
        }
    }
}
